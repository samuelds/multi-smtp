// Export module
module.exports = function() {

    /*
    *   Init vars
    */

    // Server SMTP
    var SMTPServer = require('smtp-server').SMTPServer;

    // Mail parser used for parse email
    var MailParser = require('mailparser').MailParser;

    // External
    var fs = require('fs');
    var path = require('path');
    var os = require('os');
    var tempDir = path.join(os.tmpdir(), 'maildev', process.pid.toString());

    // SMTP init
    this.smtp = null;
    this.port = null;
    this.host = null;
    this.login = null;
    this.password = null;

    /*
    *   Init functions
    */

    // Get Email data
    this.handleDataStream = function (stream, session, callback) {
      var data = {
          id: session.id,
          from: session.envelope.mailFrom,
          to: session.envelope.rcptTo,
          host: session.hostNameAppearsAs,
          remoteAddress: session.remoteAddress
      };

      function saveEmail(id, envelope, mailObject){
          console.log("save")
      }

      function createSaveStream(id, emailData) {
          var parseStream = new MailParser({
              streamAttachments: true
          });

          parseStream.on('data', data => {
              if (data.type === 'text') {
                  console.log(data);
              }

              if (data.type === 'html') {
                  console.log(data);
              }
          });

          return parseStream;
      }

      var saveRawStream = createSaveStream(data.id, data);

      stream.pipe(saveRawStream);

      stream.on('end', function(){
          callback(null, 'Message queued as ' + data.id);
      });

    }

    // Get Auth user
    this.authorizeUser = function (auth, session, callback) {

        var username = auth.username;
        var password = auth.password;

        if (this.options.incomingUser && this.options.incomingPassword) {
          if (username !== this.options.incomingUser || password !== this.options.incomingPassword) {
            return callback(new Error('Invalid username or password'));
          }
        }
        callback(null, { user: this.options.incomingUser });
    }

    // Create SMTP server
    this.create = function (port, host, user, password) {

        this.port = port;
        this.host = host;
        this.user = user;
        this.password = password;

        this.smtp = new SMTPServer ({
            incomingUser: user,
            incomingPassword: password,
            onAuth: this.authorizeUser,
            onData: this.handleDataStream,
            logger: true,
            disabledCommands: !!(user && password)?['STARTTLS']:['AUTH']
        });

    }

    this.listen = function(callback) {

      var self = this;

      if (typeof callback !== 'function') callback = null;

      // Listen on the specified port
      this.smtp.listen(self.port, self.host, function(err) {
        if (err) {
          if (callback) {
            callback(err);
          } else {
            throw err;
          }
        }

        if (callback) callback();

        console.log('MailDev SMTP Server running at %s:%s', self.host, self.port);
      });
    };
}
